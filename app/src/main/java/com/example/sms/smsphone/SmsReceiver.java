package com.example.sms.smsphone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.sms.smsphone.MainActivity.sendsms;

public class SmsReceiver extends BroadcastReceiver {
    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        Object[] pdus = (Object[]) intent.getExtras().get("pdus");//从intent内获取短信参数
        for (Object p : pdus) {
            byte[] pdu = (byte[]) p;
            SmsMessage message = SmsMessage.createFromPdu(pdu);//生成短信对象
            String content = message.getMessageBody();// 取得短信内容

            Toast.makeText(context,content,Toast.LENGTH_SHORT).show();


            Date date = new Date(message.getTimestampMillis());//获取发送时间搓
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//自定义时间格式
            String receivetime = format.format(date);
            String sender = message.getOriginatingAddress();//获取短信发送者


            MainActivity.sendsms(sender, content, receivetime);//发送WEB





        }

    }




}

